\contentsline {lstlisting}{\numberline {5.1}Extract of the \emph {Scenario\_1\_Emergencies\_Lorca\_Earthquake.csv} of the Lorca scenario}{50}{lstlisting.5.1}
\contentsline {lstlisting}{\numberline {5.2}Extract of the \emph {All\_Scenarios.csv} code of the Lorca scenario.}{50}{lstlisting.5.2}
\contentsline {lstlisting}{\numberline {5.3}Pseudocode of \emph {NotInterestingZone} function.}{59}{lstlisting.5.3}
\contentsline {lstlisting}{\numberline {5.4}Pseudocode of \emph {InterestingZone} function.}{60}{lstlisting.5.4}
\contentsline {lstlisting}{\numberline {5.5}Pseudocode of \emph {OutOfVisibility} function.}{61}{lstlisting.5.5}
\contentsline {lstlisting}{\numberline {5.6}Rspec specification for \emph {Satellite Simulators}}{72}{lstlisting.5.6}
\contentsline {lstlisting}{\numberline {5.7}Bash script to write the Database's \emph {IP address} on a file}{73}{lstlisting.5.7}
\contentsline {lstlisting}{\numberline {5.8}Rspec specification for \emph {Ground Station Simulators}}{74}{lstlisting.5.8}
\contentsline {lstlisting}{\numberline {5.9}FTP server installation}{74}{lstlisting.5.9}
\contentsline {lstlisting}{\numberline {5.10}Slice of the ICE application.}{96}{lstlisting.5.10}
